@extends('layout')

@section('title')
    {{ $title }}
@endsection('title')

@section('content')
    <h1>Hello</h1>

    <ul>
        @foreach($items as $key => $value) 
            <li>
                <strong>{{ $key }}: </strong>
                {{ $value }} 
            </li>
        @endforeach
    </ul>

@endsection('content')

