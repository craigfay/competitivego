<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title', 'defaulttitle')</title>
</head>
<body>
    <a href="/hello">Hello</a>
    <a href="/goodbye">Goodbye</a>
    @yield('content')
</body>
</html>