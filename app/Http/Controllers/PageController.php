<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function hello()
    {
        $items = [
            'name' => 'adam',
            'email' => 'adam@gmail.com',
            'hobby' => 'tennis'
        ];
    
        return view('hello', [
            'items' => $items,
            'title' => request('title')
        ]);
    }

    public function goodbye()
    {
        return view('goodbye');
    }
}
